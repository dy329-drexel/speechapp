package com.example.deepak.speechapp;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {




    private TextView result;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



   result=(TextView)findViewById(R.id.resultText);

      ImageButton  buttonAudio=(ImageButton) findViewById(R.id.imageButton);


        assert buttonAudio != null;


        //adding on click listener to our button
        buttonAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Making Speech recognizer intent and adding properties

                Intent intent=new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_PROMPT,"Please speak");
                 intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.ENGLISH);

                    startActivityForResult(intent,1);
            }
        });
    }
    public void onActivityResult(int reqCode,int resCode,Intent i){
        super.onActivityResult(reqCode,resCode,i);

        if(reqCode==1){
            if(resCode==RESULT_OK && i !=null)
            {

                //Getting data from speech recognizer and displaying it in text field

                ArrayList<String> data=i.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                result.setText(data.get(0));
            }
        }



    }
}
