Deepak Yadav
dy329@drexel.edu

Speech Application
Min android SDK 5.0

I created an application in android studio to transcript voice to text. I created a simple layout containing 
a image button with microphone picture on it , and I added onclick listener to it. I created a simple layout and code 
in an easy way to make it simple. When we click on the button It will start google speech recognizer intent with a prompt and 
defined language as English. When you speak after the speech recognizer intent it will get the audio and transcript in the textfield 
below the button.

I used google speech recognizer because it is built in Android studio and also it more precise tool for speech recognition.
I read documentaion on Recognizer intent and watch some tutorials to learn how to use speech recognizer and completed this application with everything working.
There were not so many problems apart from some difficulty in display text in a seperate fragment which the code of which I removed later.

To make this application better we can give user to select the language and then transcript it  because for now I only did it for english language only. Also we 
can also try to use google speech API for better result (If they provide better results then this) but I think that will make application slower.